package arain.noman.alphabet;

import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.ArrayList;

/*
Skipping the case where the board has the same word more than once
This case can be addressed if the business requires it and states their specific requirements.
 */
public class AlphabetSoup {
    private int row = 0;
    private int column = 0;
    private String fileName = "input.txt";
    private char[][] grid = new char[0][0];
    private InputStream inputStream = null;
    private final ArrayList<String> words = new ArrayList<>();

    // For searching in all 8 direction
    private static final int[] x = { -1, -1, -1, 0, 0, 1, 1, 1 };
    private static final int[] y = { -1, 0, 1, -1, 1, -1, 0, 1 };

    public AlphabetSoup(String fileName) {
        if(!StringUtils.isEmpty(fileName)) {
            this.fileName = fileName;
        }
        InputStream inputStream = readFile();
        parseFile(inputStream);
        for (String word : words) {
            wordSearch(word.replace("\n", ""));
        }
    }

    private InputStream readFile() {
        ClassLoader classLoader = AlphabetSoup.class.getClassLoader();
        inputStream = classLoader.getResourceAsStream(fileName);
        if (inputStream == null) {
            throw new IllegalArgumentException("File could not be found, fileName: " + fileName);
        } else {
            return inputStream;
        }
    }

    /*
    Calculates the grid size.
    Creates a grid array
    Separate the words into another list
     */
    private void parseFile(InputStream is) {
        ArrayList<String> lines = new ArrayList<>();
        try (InputStreamReader streamReader = new InputStreamReader(is);
             BufferedReader reader = new BufferedReader(streamReader)) {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line.replace(" ", "") + "\n");
            }
            getGridSize(lines.get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }

        char[][] tempGrid = new char[row][column];
        for(int i=1; i <= row; i++) {
            String tempLine = lines.get(i).toUpperCase().replace("\n", "");
            for(int a=0; a < tempLine.length(); a++) {
                tempGrid[i-1][a] = tempLine.charAt(a);
            }
        }
        this.grid = tempGrid;
        // Separate out the words list
        for(int i=row+1; i < lines.size(); i++) {
            words.add(lines.get(i));
        }
    }

    private void getGridSize(String size) {
        if (StringUtils.isEmpty(size) || !StringUtils.containsIgnoreCase(size, "x")) {
            return;
        }
        size = size.replace("\n", "");
        String[] parts = size.split("x");
        String part1 = parts[0];
        String part2 = parts[1];
        this.row = Integer.parseInt(part1);
        this.column = Integer.parseInt(part2);
    }

    public boolean search2D(char[][] grid, int row, int col, String word)
    {
        word = word.toUpperCase();
        if (grid[row][col] != word.charAt(0)) {
            return false;
        }
        int wordLength = word.length();
        // Search word in all 8 directions
        // starting from (row, col)
        for (int direction = 0; direction < 8; direction++) {
            // Initialize starting point
            // for current direction
            int k = 0;
            int rd = row + x[direction];
            int cd = col + y[direction];

            // First character is already checked,
            // match remaining characters
            for (k = 1; k < wordLength; k++) {
                // If out of bound break
                if (rd >= this.row || rd < 0 || cd >= this.column || cd < 0) {
                    break;
                }

                // If not matched, break
                if (grid[rd][cd] != word.charAt(k)) {
                    break;
                }

                // Moving in particular direction
                rd += x[direction];
                cd += y[direction];
            }

            // If all character matched,
            // then value of must
            // be equal to length of word
            if (k == wordLength) {
                return true;
            }
        }
        return false;
    }

    // Searches given word in a given
    // matrix in all 8 directions
    private void wordSearch(String word)
    {
        StringBuilder wordBuilder = new StringBuilder();
        String wordReversed = wordBuilder.append(word).reverse().toString();
        String coordStart = null;
        String coordEnd = null;
        if (StringUtils.equals(word, wordReversed)) {
            coordStart = startThroughGrid(word, false);
            coordEnd = startThroughGrid(wordReversed, true);
        } else {
            coordStart = startThroughGrid(word, false);
            coordEnd = startThroughGrid(wordReversed, false);
        }

        if (!StringUtils.isEmpty(coordStart) && !StringUtils.isEmpty(coordEnd)) {
            System.out.println(word + " " + coordStart + " " + coordEnd);
        }
    }

    private String startThroughGrid(String word, boolean palindrome) {
        if (palindrome) {
            for (int row = this.row-1; row >= 0; row--) {
                for (int col = this.column-1; col >= 0; col--) {
                    if (search2D(this.grid, row, col, word)) {
                        return row + ":" + col;
                    }
                }
            }
        } else {
            for (int row = 0; row < this.row; row++) {
                for (int col = 0; col < this.column; col++) {
                    if (search2D(this.grid, row, col, word)) {
                        return row + ":" + col;
                    }
                }
            }
        }
        return null;
    }

    public static void main(String[] args)
    {
        String fName = null;
        if (null != args && args.length > 0) {
            fName = args[0];
        }
        new AlphabetSoup(fName);
    }
}
